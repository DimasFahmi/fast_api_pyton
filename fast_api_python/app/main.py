from fastapi import FastAPI

# Create an instance of FastAPI
app = FastAPI()

# Define a route and a function that handles it
@app.get("/")
async def read_root():
    return {"message": "Hello, FastAPI!"}